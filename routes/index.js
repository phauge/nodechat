var	util	=	require('../middleware/utilities');
var	io	=	require('socket.io'),
		connect	=	require('connect'),
		cookie	=	require('cookie'),
		expressSession	=	require('express-session'),
		ConnectRedis	=	require('connect-redis')(expressSession),
		redis	=	require('redis'),
		config	=	require('../config'),
		redisSession	=	new	ConnectRedis({host:	config.redisHost,	port:config.redisPort});
var	user	=	require('../passport/user');

var	socketAuth	=	function	socketAuth(socket,	next){
	next();
};

var	socketConnection	=	function	socketConnection(socket){
		socket.emit('message',	{message:	'Hey!'});
		socket.emit('message',	socket.user);
};

exports.startIo	=	function	startIo(server){
		var io = require('socket.io')(server);
		var	packtchat	=	io.of('/packtchat');
		packtchat.use(socketAuth);
		packtchat.on('error',	function(data){
			console.log(data);
		});
		packtchat.on('connection',	socketConnection);
		return	io;
};

exports.index	=	function	index(req,	res){
		res.render('index',	{title:	'Index'});
};

exports.login	=	function	login(req,	res){
		res.render('login',	{title:	'Login',	message:	req.flash('error')});
};

exports.chat	=	function	chat(req,	res){
	res.render('chat',	{title:	'Chat'});
};

exports.logOut	=	function	logOut(req,	res){
		util.logOut(req);
		res.redirect('/');
};

module.exports.register	=	register;
module.exports.registerProcess	=	registerProcess;

//functions
function	register(req,	res){
	res.render('register',	{title:	'Register',	message:	req.flash('error')});
};

function	registerProcess	(req,	res){
	if	(req.body.username	&&	req.body.password) {
		user.addUser(req.body.username,	req.body.password, config.crypto.workFactor,	function(err,	profile){
			if	(err)	{
					req.flash('error',	err);
					res.redirect(config.routes.register);
			}else{
				req.login(profile,	function(err){
					res.redirect('/chat');
				});
			}
		});
	}else{
		req.flash('error',	'Please	fill	out	all	the	fields');
		res.redirect(config.routes.register);
	}
};
